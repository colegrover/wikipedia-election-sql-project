import java.sql.*;
import java.io.*;

public class TestMySQL
{
   
  public static void main (String args [])
      throws SQLException, IOException {

    try {
 Class.forName("com.mysql.jdbc.Driver").newInstance();
} catch (Exception ex) {}

 String url = "database_url";

 Connection con = DriverManager.getConnection(url,"username", "password");
 Statement stmt = con.createStatement ();
  
   String Input1 = readEntry("Enter user id of user to display their total votes cast opposed, neutral, and in support): ");
   int input1 = Integer.parseInt(Input1);
   FindVoteTotalID(input1);
   
   String Input2 = readEntry("Enter screen name of user to display their total votes cast opposed, neutral, and in support: ");
   FindVoteTotalNAME(Input2);
  
   String Input3 = readEntry("Enter user id of nominated user to display the username(s) and ID(s) of their Nominator(s): ");
   int input3 = Integer.parseInt(Input3);
   FindNoms(input3);
   
    String Input4 = readEntry("Enter screen name of nominated user to display total votes cast opposed, neutral, in support and the election result (0 = fail, 1 = pass): ");
   FindVotes(Input4);
   
    con.close();
  }
  static String readEntry(String prompt) {
     try {
       StringBuffer buffer = new StringBuffer();
       System.out.print(prompt);
       System.out.flush();
       int c = System.in.read();
       while(c != '\n' && c != -1) {
         buffer.append((char)c);
         c = System.in.read();
       }
       return buffer.toString().trim();
     } catch (IOException e) {
       return "";
       }
   }
   
   public static void FindVoteTotalID(int user_id )
   throws SQLException, IOException {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } 
        catch (Exception ex) {
        }
        String url = "database_url";
        Connection con = DriverManager.getConnection(url,"username", "password");
        Statement stmt = con.createStatement ();
        
        ResultSet rset = stmt.executeQuery
        
        ("SELECT SUM(Vote = '-1') as 'Oppose', SUM(Vote='0') as 'Neutral', SUM(Vote='1') as 'Support' from Vote where User_id ="+user_id);
       
        ResultSetMetaData rsmd = rset.getMetaData();
        
        while(rset.next()) {
            if (rset.getString(1)==null) {
            System.out.println("Wikipedia user has not cast any votes.");
            return;
        }
             System.out.println(
             rsmd.getColumnName(1) + "      " +
                         rsmd.getColumnName(2) + "      " + rsmd.getColumnName(3));
        System.out.println(
             rset.getString(1) + "           " +
                         rset.getString(2) + "            " + rset.getString(3));
           } 
    }
    
   public static void FindVoteTotalNAME(String user_name )
        throws SQLException, IOException {
       try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } 
        catch (Exception ex) {
        }
        String url = "database_url";
        Connection con = DriverManager.getConnection(url,"username", "password");
        Statement stmt = con.createStatement ();
        
        ResultSet rset = stmt.executeQuery
        ("SELECT SUM(Vote = '-1') as 'Oppose', SUM(Vote='0') as 'Neutral', SUM(Vote='1') as 'Support' from Vote where Username="+"'"+user_name+"'");
       
      ResultSetMetaData rsmd = rset.getMetaData();
        
        while(rset.next()) {
            if (rset.getString(1)==null) {
            System.out.println("Wikipedia user has not cast any votes.");
            return;
        }
             System.out.println(
             rsmd.getColumnName(1) + "      " +
                         rsmd.getColumnName(2) + "      " + rsmd.getColumnName(3));
        System.out.println(
             rset.getString(1) + "           " +
                         rset.getString(2) + "            " + rset.getString(3));
           } 
        }
    
    public static void FindNoms(int euser_id )
   throws SQLException, IOException {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } 
        catch (Exception ex) {
        }
        String url = "database_url";
        Connection con = DriverManager.getConnection(url,"username", "password");
        Statement stmt = con.createStatement ();
        
        ResultSet rset = stmt.executeQuery
        ("select Nominator_username,Nominator_id FROM Election WHERE Editor_id =" + euser_id);
        ResultSetMetaData rsmd = rset.getMetaData();
        if (rset.next()) {
            do {
                System.out.println(
                rsmd.getColumnName(1) + "      " +
                         rsmd.getColumnName(2));
           System.out.println(
             rset.getString(1) + "                   " +
                         rset.getString(2));
            } while(rset.next());
          } else {
             System.out.println("Wikipedia user has not been nominated.");
          }              
     }
     
     public static void FindVotes(String euser_id )
   throws SQLException, IOException {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } 
        catch (Exception ex) {
        }
        String url = "database_url";
        Connection con = DriverManager.getConnection(url,"username", "password");
        Statement stmt = con.createStatement ();
        
        ResultSet rset = stmt.executeQuery
        ("SELECT SUM(Vote = '-1') as 'Oppose', SUM(Vote='0') as 'Neutral', SUM(Vote='1') as 'Support',Success from Vote where Editor_username =" + "'"+euser_id+"'");
        
        ResultSetMetaData rsmd = rset.getMetaData();
        
        while(rset.next()) {
            if (rset.getString(1)==null) {
            System.out.println("Wikipedia user has not been nominated.");
            return;
        }
             System.out.println(
             rsmd.getColumnName(1) + "      " +
                         rsmd.getColumnName(2) + "      " + rsmd.getColumnName(3) + "       " + rsmd.getColumnName(4));
        System.out.println(
             rset.getString(1) + "           " +
                         rset.getString(2) + "            " + rset.getString(3) + "            " + rset.getString(4));
           } 
   }
}