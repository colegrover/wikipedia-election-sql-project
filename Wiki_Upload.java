import java.sql.*;
import java.io.*;
import java.util.Scanner;
import java.util.Arrays;

public class TestMySQL
{
   
  public static void main (String args [])
      throws SQLException, IOException {

    try {

Class.forName("com.mysql.jdbc.Driver").newInstance();
} catch (Exception ex) {

}

 String url = "jdbc:mysql://codd.cs.luc.edu/cgrover_wiki_elections";

 Connection con = DriverManager.getConnection(url,"cgrover", "p82193");
    Statement stmt = con.createStatement ();
  
        Scanner scanner = new Scanner(new File("/Users/Cole/Downloads/WikiFULL.txt"));
         
        scanner.useDelimiter("\\t");
         
       String[] Election = new String[6];
       String[] Vote = new String[7];
       
try {
    while ( scanner.hasNextLine() ){
      
        String[] tokens = scanner.nextLine().split("\\t");
        String key = tokens[0];

        if (Arrays.asList(tokens).contains("E")){
            
            String success = String.valueOf(tokens[1]);
            Election[4] = success;
            Vote[6] = success;
            
        }
        if (Arrays.asList(tokens).contains("T")){
            
            String Etime = String.valueOf(tokens[1]);
            Election[0] = Etime;
            Vote[3] = Etime;
            
        }
        if (Arrays.asList(tokens).contains("U")){
            
            String E_un = String.valueOf(tokens[2]);
            String E_id = String.valueOf(tokens[1]);
            
            Election[1] = E_id;
            
            Vote[4] = E_un;
            
        }
        if (Arrays.asList(tokens).contains("N")){
            
            String N_un = String.valueOf(tokens[2]);
            String N_id = String.valueOf(tokens[1]);
            
            Election[2] = N_un;
            Election[3] = N_id;
            
            int E_idn = Integer.parseInt(Election[1]);
            int N_idne = Integer.parseInt(Election[3]);
            
            int successn = Integer.parseInt(Election[4]);
            
            //System.out.println(Arrays.toString(Election));
            
            String sql1 = "INSERT INTO Election (Election_time, Editor_id, Nominator_username, Nominator_id, Success)" + "VALUES(?,?,?,?,?)";
                            PreparedStatement pstmt1 = con.prepareStatement(sql1);
                            pstmt1.setString(1,Election[0]);
                            pstmt1.setInt(2,E_idn);
                            pstmt1.setString(3,Election[2]);
                            pstmt1.setInt(4,N_idne);
                            pstmt1.setInt(5,successn);
    
                            pstmt1.executeUpdate();
           
        }
        if (Arrays.asList(tokens).contains("V")){
            
            String Un = String.valueOf(tokens[4]);
            String U_id = String.valueOf(tokens[2]);
            String V_time = String.valueOf(tokens[3]);
            String Vote_value = String.valueOf(tokens[1]);
            
            Vote[0] = V_time;
            Vote[1] = Un;
            Vote[2] = U_id;
            Vote[5] = Vote_value;
            
            int U_idnn = Integer.parseInt(Vote[2]);
            int successv = Integer.parseInt(Vote[6]);
           
            //System.out.println(Arrays.toString(Vote));
            
            String sql2 = "INSERT INTO Vote (Vote_time, Username, User_id, Election_time, Editor_username, Vote, Success)" + "VALUES(?,?,?,?,?,?,?)";
                            PreparedStatement pstmt2 = con.prepareStatement(sql2);
                            pstmt2.setString(1,Vote[0]);
                            pstmt2.setString(2,Vote[1]);
                            pstmt2.setInt(3,U_idnn);
                            pstmt2.setString(4,Vote[3]);
                            pstmt2.setString(5,Vote[4]);
                            pstmt2.setString(6,Vote[5]);
                            pstmt2.setInt(7,successv);
    
                            pstmt2.executeUpdate();

        }
    }
    System.out.println("Upload Complete!");
}
finally {
    scanner.close();
}   
    con.close();
  }

  static String readEntry(String prompt) {
     try {
       StringBuffer buffer = new StringBuffer();
       System.out.print(prompt);
       System.out.flush();
       int c = System.in.read();
       while(c != '\n' && c != -1) {
         buffer.append((char)c);
         c = System.in.read();
       }
       return buffer.toString().trim();
     } catch (IOException e) {
       return "";
       }
   }
}