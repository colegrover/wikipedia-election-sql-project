use cgrover_wiki_elections;

CREATE TABLE Election(
Election_time DATETIME NOT NULL,
Editor_id int NOT NULL,
Nominator_username VARCHAR(40),
Nominator_id int,
Success int,
PRIMARY KEY (Election_time,Editor_id)
);      

CREATE TABLE Vote(
Vote_time DATETIME NOT NULL,
Username VARCHAR(40) NOT NULL,
User_id int,
Election_time DATETIME NOT NULL,
Editor_username VARCHAR(40) NOT NULL,
Vote VARCHAR(2),
Success int,
PRIMARY KEY (Vote_time,Username, Election_time, Editor_username),
FOREIGN KEY (Election_time) REFERENCES Election(Election_time)
); 