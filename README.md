This is a copy of a final project in my Database Programming class at Loyola University Chicago.

I was tasked with creating a SQL database to store Wikipedia election and vote data for wikipedia elections where editors applied to become administrators. This data was then read by a java program which parsed and uploaded it to the SQL database. Another java file acts as a simple interface that allows you to make several simple SQL queries 

List of files and their purpose:

* Wiki_DDL.sql - Creates SQL tables
* Wiki_Data_FULL.txt - The full wikipedia administrator dataset.
* Wiki_Data_Sample.txt - A small version of the dataset for testing.
* Wiki_Queries.java - Java console interface to query database.
* Wiki_Schema.pdf - SQL database schema.
* Wiki_Upload.java - Java program that reads, parses, and uploads the dataset.

To use this program, download the files and alter the JDBC connection information to match your database in Wiki_Queries and Wiki-Upload. Include the correct path for the scanner to read the Wiki_Data_Full dataset. You will need the JDBC driver to properly run the upload file.

Create the tables in your database, run the upload in your preferred IDE, then run the queries file to query your database.